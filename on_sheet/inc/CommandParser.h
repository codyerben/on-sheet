////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Definition oof the CommandParser class.

*/

////////////////////////////////////////////////////////////////////////////////
#ifndef COMMAND_PARSER_H
#define COMMAND_PARSER_H

#include "ICommandParser.h"

class CommandParser : public ICommandParser
{
    public:

        CommandParser();
        virtual ~CommandParser();

        virtual Command RetrieveCommand(const std::string& arguments);

    protected:

        // disabled constructors
        CommandParser(const CommandParser& x) = delete;
        CommandParser& operator=(const CommandParser& x) = delete;

};

#endif
