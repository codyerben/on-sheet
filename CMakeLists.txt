cmake_minimum_required(VERSION 3.0)

set(This on_sheet)
project(${This} C CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(ON_SHEET_MODULE on_sheet)

add_subdirectory(${ON_SHEET_MODULE})
add_subdirectory(test)

# Download GTEST and GMOCK
# Code for this task from: https://github.com/Crascit/DownloadProject

# GTEST and GMOCK got combined into a single project

include(DownloadProject.cmake)
download_project(PROJ                googletest
                 GIT_REPOSITORY      https://github.com/google/googletest.git
                 GIT_TAG             master
                 PREFIX              ${CMAKE_SOURCE_DIR}/ext_modules           
)

# Prevent GoogleTest from overriding our compiler/linker options
# when building with Visual Studio
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
